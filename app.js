var http = require('http');
var dept = require('./dept');
var empl = require('./emp/emitter');

var hostname = '127.0.0.1';
var port = 3000;

var server = http.createServer(function(req, res) {
  var bakingDept = dept.createDepartment();
  bakingDept.addEmployee('John');
  bakingDept.addEmployee('Jane');

  res.statusCode = 200;
  res.setHeader('Content-Type', 'text/plain');
  res.end('Employees count: ' + bakingDept.employeeNames.length + '\n');
});

server.listen(port, hostname, function() {
  console.log('Server running at http://' + hostname + ':' + port);
});
