function Department() {}

Department.prototype.employeeNames = [];

Department.prototype.addEmployee = function(name) {
    if (!name) {
        throw Error('Employee name is not specified.');
    }
    var lowerName = name.toLowerCase();
    var names = this.employeeNames || [];
    for (var n in names) {
        if (names[n].toLowerCase() === lowerName) {
            throw new Error('Employee with name "' + name + '" exists.');
        }
    }
    this.employeeNames.push(name);
};

module.exports = Department;
