var EventEmitter = require('events');

class MyEmitter extends EventEmitter {};

var myEmitter = new MyEmitter();

myEmitter.on('generate', function() {
    console.log('the number generated!');
});

myEmitter.emit('generate');
